package main

import (
	"bufio"
	"strconv"
	"strings"
)

func init() {
	intFuncs = append(intFuncs, doTwo)
}

func doTwo(s *bufio.Scanner) (int, error) {
	paths := map[point]map[int]int{}
	cur := &point{}
	wireNo := 0
	stepCount := 0
	for ; s.Scan(); wireNo++ {
		l := s.Text()
		wire := strings.Split(l, ",")

		for _, move := range wire {
			dir := move[0]
			steps, err := strconv.Atoi(move[1:])
			if err != nil {
				return 0, err
			}
			moves := cur.move(dir, steps)
			for _, m := range moves {
				if _, ok := paths[m]; ok {
					if paths[m][wireNo] == 0 {
						paths[m][wireNo] = stepCount
					}
				} else {
					paths[m] = map[int]int{wireNo: stepCount}
				}
				stepCount++
			}
		}
		cur = &point{}
		stepCount = 0
	}
	delete(paths, point{0, 0})
	min := 0
	for _, w := range paths {
		if len(w) == wireNo {
			s := 0
			for _, v := range w {
				s += v
			}
			if s < min || min == 0 {
				min = s
			}
		}
	}

	return min, nil
}
