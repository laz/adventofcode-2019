package main

import (
	"bufio"
	"strconv"
	"strings"
)

func init() {
	intFuncs = append(intFuncs, doOne)
}

func doOne(s *bufio.Scanner) (int, error) {
	paths := map[point]map[int]bool{}
	cur   := &point{}
	wireNo := 0
	for ; s.Scan(); wireNo++ {
		l := s.Text()
		wire := strings.Split(l, ",")

		for _, move := range wire {
			dir := move[0]
			steps, err := strconv.Atoi(move[1:])
			if err != nil {
				return 0, err
			}
			moves := cur.move(dir, steps)
			for _, m := range moves {
				if _, ok := paths[m]; ok {
					paths[m][wireNo] = true
				} else {
					paths[m] = map[int]bool{wireNo: true}
				}
			}
		}
		cur = &point{}
	}
	min := 0
	central := &point{0, 0}
	for p, w := range paths {
		if len(w) == wireNo {
			d := p.distance(central)
			if (d < min && d != 0) || min == 0 {
				min = d
			}
		}
	}

	return min, nil
}
