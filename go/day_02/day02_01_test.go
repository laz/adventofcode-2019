package main

import (
	"bufio"
	"bytes"
	"testing"
)

func TestDoOne(t *testing.T) {
	tests := map[string]int{
		"1,0,0,0,99":     2,
		"2,3,0,3,99":     6,
		"2,4,4,5,99,0":   9801,
		"1,1,1,4,99,5,6,0,99": 30,
	}

	for test, expect := range tests {
		actual, err := doOne(bufio.NewScanner(bytes.NewBufferString(test)))

		if err != nil {
			t.Errorf("unexpected error on input %s: %s", test, err)
		}
		if actual != expect {
			t.Errorf("failed on input %s: got %d, expected %d", test, actual, expect)
		}
	}
}
