package main

import (
	"bufio"
	"strings"
)

func init() {
	intFuncs = append(intFuncs, doOne)
}

func doOne(s *bufio.Scanner) (int, error) {
	m := map[string]*orbiter{"COM": &orbiter{id: "COM"}}
	o := []*orbiter{m["COM"]}
	for s.Scan() {
		l := s.Text()
		pair := strings.Split(l, ")")

		a := m[pair[0]]
		if a == nil {
			a = &orbiter{id: pair[0]}
			m[pair[0]] = a
			o = append(o, a)
		}
		b := m[pair[1]]
		if b == nil {
			b = &orbiter{id: pair[1]}
			m[pair[1]] = b
			o = append(o, b)
		}
		a.addOrbit(b)
	}

	tot := 0
	for _, cur := range o {
		sum := 0
		orb := m[cur.id]
		for orb != nil {
			orb = orb.orbitting
			if orb != nil {
				sum++
			}
		}
		tot += sum
	}
	return tot, nil
}
