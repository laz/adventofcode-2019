package main

import (
	"bufio"
)

func init() {
	intFuncs = append(intFuncs, doTwo)
}

const target = 19690720

func doTwo(s *bufio.Scanner) (int, error) {
	prog, err := input(s)
	if err != nil {
		return 0, err
	}

	res := -1
	for n := 0; n <= 99; n++ {
		for v := 0; v <= 99; v++ {
			attempt := make([]int, len(prog))
			copy(attempt, prog)
			attempt[1] = n
			attempt[2] = v

			test, err := process(attempt)
			if err == nil && test == target {
				res = n*100 + v
				break
			}
		}
		if res > -1 {
			break
		}
	}
	return res, nil
}
