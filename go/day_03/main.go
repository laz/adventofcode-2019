package main

import (
	"gitlab.com/laz/adventofcodeutil"
	"fmt"
)

var stringFuncs = []adventofcodeutil.StringFuncScanner{}
var intFuncs = []adventofcodeutil.IntFuncScanner{}

func main() {
	for _, f := range stringFuncs {
		res, err := adventofcodeutil.ExecuteStringFuncScanner(f)
		if err != nil {
			panic(err)
		}
		fmt.Println(res)
	}
	for _, f := range intFuncs {
		res, err := adventofcodeutil.ExecuteIntFuncScanner(f)
		if err != nil {
			panic(err)
		}
		fmt.Println(res)
	}
}
