package main

import (
	"bufio"
	"bytes"
	"testing"
)

func TestDoOne(t *testing.T) {
	tests := map[string]int{
		"12":     2,
		"14":     2,
		"1969":   654,
		"100756": 33583,
	}

	for test, expect := range tests {
		actual, err := doOne(bufio.NewScanner(bytes.NewBufferString(test)))

		if err != nil {
			t.Errorf("unexpected error on input %s: %s", test, err)
		}
		if actual != expect {
			t.Errorf("failed on input %s: got %d, expected %d", test, actual, expect)
		}
	}
}
