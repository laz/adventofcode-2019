package intcode

type JumpConditionOp struct {
	OpcodeBase
	jump   int64
	doJump bool
	f      func(a, b int64) int64
}

func (a *JumpConditionOp) Execute() error {
	first, err := a.GetParameter(0)
	if err != nil {
		return err
	}
	second, err := a.GetParameter(1)
	if err != nil {
		return err
	}
	a.jump = a.f(first, second)
	if a.outMode == StoreOutput {
		a.output = a.jump
	}
	return nil
}

func (a *JumpConditionOp) NextPosition() (int64, bool) {
	if a.doJump {
		return a.jump, true
	}
	return a.OpcodeBase.NextPosition()
}

type JumpIfTrueOp struct {
	JumpConditionOp
}

func (a *JumpIfTrueOp) Execute() error {
	a.f = func(i, j int64) int64 {
		if i != 0 {
			a.doJump = true
			return j
		}
		return 0
	}
	return a.JumpConditionOp.Execute()
}

type JumpIfFalseOp struct {
	JumpConditionOp
}

func (a *JumpIfFalseOp) Execute() error {
	a.f = func(i, j int64) int64 {
		if i == 0 {
			a.doJump = true
			return j
		}
		return 0
	}
	return a.JumpConditionOp.Execute()
}
