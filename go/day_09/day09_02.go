package main

import (
	"bufio"

	"gitlab.com/laz/adventofcode/2019/go/intcode"
)

func init() {
	intFuncs = append(intFuncs, doTwo)
}

func doTwo(s *bufio.Scanner) (int, error) {
	s.Scan()
	l := s.Text()
	p, err := intcode.ParseProgram(l)
	if err != nil {
		return 0, err
	}

	in := &intcode.ArrayProgramInput{In: []int64{2}}
	prog := &intcode.Program{Program: p, OutMode: intcode.IgnoreOutput, In: in}
	r, err := prog.Execute()

	sum := int64(0)
	for _, i := range r {
		sum += i
	}
	return int(sum), err
}
