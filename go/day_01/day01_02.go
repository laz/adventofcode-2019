package main

import (
	"bufio"
	"strconv"
)

func init() {
	intFuncs = append(intFuncs, doTwo)
}

func doTwo(s *bufio.Scanner) (int, error) {
	total := 0
	for s.Scan() {
		l := s.Text()
		i, err := strconv.ParseInt(l, 10, 64)
		if err != nil {
			return 0, err
		}
		for i > 5 {
			i = i/3 - 2
			total += int(i)
		}
	}
	return total, nil
}
