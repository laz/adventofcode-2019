package intcode

type AddOpcode struct {
	ThreeArgFuncOpcode
}

func (a *AddOpcode) Execute() error {
	a.f = func(i, j int64) int64 { return i + j }
	return a.ThreeArgFuncOpcode.Execute()
}

type MulOpcode struct {
	ThreeArgFuncOpcode
}

func (a *MulOpcode) Execute() error {
	a.f = func(i, j int64) int64 { return i * j }
	return a.ThreeArgFuncOpcode.Execute()
}

type LessThanOpcode struct {
	ThreeArgFuncOpcode
}

func (a *LessThanOpcode) Execute() error {
	a.f = func(i, j int64) int64 {
		if i < j {
			return 1
		}
		return 0
	}
	return a.ThreeArgFuncOpcode.Execute()
}

type EqualsOpcode struct {
	ThreeArgFuncOpcode
}

func (a *EqualsOpcode) Execute() error {
	a.f = func(i, j int64) int64 {
		if i == j {
			return 1
		}
		return 0
	}
	return a.ThreeArgFuncOpcode.Execute()
}
