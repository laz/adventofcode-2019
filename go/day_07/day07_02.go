package main

import (
	"bufio"
	"fmt"
	"gitlab.com/laz/adventofcode/2019/go/intcode"
)

func init() {
	intFuncs = append(intFuncs, doTwo)
}

func doTwo(s *bufio.Scanner) (int, error) {
	max := int64(0)
	s.Scan()
	l := s.Text()

	p, err := intcode.ParseProgram(l)
	if err != nil {
		return 0, err
	}
	phaseSettingCandidates := []int64{5, 6, 7, 8, 9}

	tests := permutations(phaseSettingCandidates)
	for _, test := range tests {
		candidate := int64(0)
		res := int64(0)
		count := len(phaseSettingCandidates)
		programs := make([]*intcode.Program, count)
		for i := range programs {
			program := make(map[int64]int64, len(p))
			for k, v := range p {
				program[k] = v
			}
			in := &intcode.ArrayProgramInput{In: []int64{test[i]}}
			programs[i] = &intcode.Program{Program: program, OutMode: intcode.FeedbackLoop, In: in}
		}

		done := false
		completions := make([]bool, count)
		for amp := 0; !done; amp = (amp + 1) % count {
			if !completions[amp] {
				p := programs[amp]
				p.AppendInput(res)
				_, err = p.Execute()
				if err != nil {
					return 0, fmt.Errorf("error processing amp %d test %v with input %d: %s", amp, test, res, err)
				}

				res = p.Output[len(p.Output)-1]
				if amp == count-1 && res > 0 {
					candidate = res
				}

				if p.Status() == intcode.ProgramCompleted {
					completions[amp] = true
				}
				check := 0
				for _, b := range completions {
					if b {
						check++
					}
				}
				done = check == count
			}
		}
		if candidate > max {
			max = candidate
		}
	}

	return int(max), nil
}
