package main

import (
	"bufio"
	"fmt"
	"strconv"
	"strings"
)

func input(s *bufio.Scanner) ([]int, error) {
	s.Scan()
	val := strings.Split(s.Text(), ",")
	prog := make([]int, len(val))

	for i, v := range val {
		j, err := strconv.Atoi(v)
		if err != nil {
			return nil, err
		}
		prog[i] = j
	}
	return prog, nil
}

func process(prog []int) (int, error) {
	pos := 0
	count := len(prog)
	res := count + 1
	for {
		if pos >= count {
			return 0, fmt.Errorf("length problem finding op: position %d, length %d, %v", pos, count, prog)
		}

		op := prog[pos]
		if op == 99 {
			break
		}
		if pos+3 >= count {
			return 0, fmt.Errorf("length problem executing op %d: position %d, length %d, %v", op, pos, count, prog)
		}

		a, b, c := prog[pos+1], prog[pos+2], prog[pos+3]
		if a >= count || b >= count || c >= count {
			return 0, fmt.Errorf("invalid index: %d %d %d, length %d, %v", a, b, c, count, prog)
		}
		switch op {
		case 1:
			prog[c] = prog[b] + prog[a]
			res = c
		case 2:
			prog[c] = prog[b] * prog[a]
			res = c
		default:
			return 0, fmt.Errorf("invalid op code: position %d, code %d, %v", pos, op, prog)
		}
		pos += 4
	}

	if res >= count {
		fmt.Errorf("invalid result index: %d, length %d, %v", res, count, prog)
	}
	return prog[res], nil
}
