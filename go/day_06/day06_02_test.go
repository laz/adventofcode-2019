package main

import (
	"bufio"
	"bytes"
	"testing"
)

func TestDoTwo(t *testing.T) {
	tests := map[string]int{
		"COM)B\nB)C\nC)D\nD)E\nE)F\nB)G\nG)H\nD)I\nE)J\nJ)K\nK)L\nK)YOU\nI)SAN": 4,
	}

	for test, expect := range tests {
		actual, err := doTwo(bufio.NewScanner(bytes.NewBufferString(test)))

		if err != nil {
			t.Errorf("unexpected error on input %s: %s", test, err)
		}
		if actual != expect {
			t.Errorf("failed on input %s: got %d, expected %d", test, actual, expect)
		}
	}
}
