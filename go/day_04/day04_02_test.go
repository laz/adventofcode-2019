package main

import (
	"bufio"
	"bytes"
	"testing"
)

func TestDoTwo(t *testing.T) {
	tests := map[string]int{
		"111122-111122": 1,
		"112233-112233": 1,
		"123444-123444": 0,
	}

	for test, expect := range tests {
		actual, err := doTwo(bufio.NewScanner(bytes.NewBufferString(test)))

		if err != nil {
			t.Errorf("unexpected error on input %s: %s", test, err)
		}
		if actual != expect {
			t.Errorf("failed on input %s: got %d, expected %d", test, actual, expect)
		}
	}
}
