package main

import (
	"bufio"
	"strconv"
)

func init() {
	intFuncs = append(intFuncs, doOne)
}

func doOne(s *bufio.Scanner) (int, error) {
	total := 0
	for s.Scan() {
		l := s.Text()
		i, err := strconv.ParseInt(l, 10, 64)
		if err != nil {
			return 0, err
		}
		total += int(i/3 - 2)
	}
	return total, nil
}
