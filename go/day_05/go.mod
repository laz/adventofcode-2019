module aoc-2019-day05

go 1.12

require (
	gitlab.com/laz/adventofcode/2019/go/intcode v0.0.0-00010101000000-000000000000
	gitlab.com/laz/adventofcodeutil v0.0.0-20191201175257-11e5e71f28b1
)

replace gitlab.com/laz/adventofcode/2019/go/intcode => ../intcode
