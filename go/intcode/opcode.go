package intcode

import "fmt"

type ParameterMode int

const (
	PositionParameterMode ParameterMode = iota
	ImmediateParameterMode
	RelativeParameterMode
)

type Opcode interface {
	Execute() error
	NextPosition() (int64, bool)
	OpID() int
	OutputValue() (int64, bool)
	ParameterCount() int64
	ResultingProgramStatus() ExecutionStatus
}

type OpcodeBase struct {
	id                     int
	input                  int64
	output                 int64
	outMode                OutputMode
	parameterModes         []ParameterMode
	parameters             map[int64]int64
	position               int64
	program                map[int64]int64
	programLength          int64
	relativeBase           int64
	resultingProgramStatus ExecutionStatus
}

func (o *OpcodeBase) GetParameter(i int64) (int64, error) {
	param := o.parameters[i]
	mode := PositionParameterMode
	if i >= 0 && i < int64(len(o.parameterModes)) {
		mode = o.parameterModes[i]
	}
	if mode != ImmediateParameterMode {
		if mode == RelativeParameterMode {
			param = o.relativeBase + param
		}
		if param < 0 {
			return param, fmt.Errorf("encountered negative position: op %d, current position %d, negative %d", o.id, o.position, param)
		}
		param = o.program[param]
	}
	return param, nil
}

func (o *OpcodeBase) NextPosition() (int64, bool) {
	return o.position + o.ParameterCount() + 1, false
}

func (o *OpcodeBase) OpID() int {
	return o.id
}

func (o *OpcodeBase) OutputValue() (int64, bool) {
	if o.outMode == IgnoreOutput {
		return 0, false
	}
	return o.output, true
}

func (a *OpcodeBase) Execute() error {
	return nil
}

func (o *OpcodeBase) ParameterCount() int64 {
	return int64(len(o.parameters))
}

func (o *OpcodeBase) String() string {
	return fmt.Sprintf("%d %v %v", o.id, o.parameterModes, o.parameters)
}

func (o *OpcodeBase) ResultingProgramStatus() ExecutionStatus {
	return o.resultingProgramStatus
}

type ThreeArgFuncOpcode struct {
	OpcodeBase
	f func(a, b int64) int64
}

func (a *ThreeArgFuncOpcode) Execute() error {
	first, err := a.GetParameter(0)
	if err != nil {
		return err
	}
	second, err := a.GetParameter(1)
	if err != nil {
		return err
	}
	third := a.parameters[2]
	if len(a.parameterModes) >= 3 && a.parameterModes[2] == RelativeParameterMode {
		third += a.relativeBase
		if third < 0 {
			return fmt.Errorf("encountered negative position: op %d, current position %d, negative %d", a.id, a.position, third)
		}
	}
	a.program[third] = a.f(first, second)
	if a.outMode == StoreOutput {
		a.output = a.program[third]
	}
	return nil
}
