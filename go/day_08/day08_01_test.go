package main

import (
	"testing"
)

func TestDoOne(t *testing.T) {
	tests := map[string]int{
		"123456789012": 1,
	}

	for test, expect := range tests {
		actual := calc(test, 3, 2)

		if actual != expect {
			t.Errorf("failed on input %s: got %d, expected %d", test, actual, expect)
		}
	}
}
