package intcode

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
)

type OutputMode int

const (
	IgnoreOutput OutputMode = iota
	StoreOutput
	FeedbackLoop
)

type ExecutionStatus int

const (
	ProgramNotStarted ExecutionStatus = iota
	ProgramRunning
	ProgramAwaitingInput
	ProgramCompleted
	ProgramSentOutput
)

type Program struct {
	Program map[int64]int64
	OutMode OutputMode
	In      ProgramInput
	Output  []int64

	position     int64
	relativeBase int64
	status       ExecutionStatus
}

func (p *Program) Status() ExecutionStatus {
	return p.status
}

func (p *Program) AppendInput(input int64) {
	p.In.Append(input)
}

type ProgramInput interface {
	Next() (int64, bool)
	Append(int64)
}

type ArrayProgramInput struct {
	In      []int64
	current int
}

func (a *ArrayProgramInput) Next() (input int64, ok bool) {
	if a != nil && a.current < len(a.In) {
		input = a.In[a.current]
		a.current++
		ok = true
	}
	return
}

func (a *ArrayProgramInput) Append(input int64) {
	if a != nil {
		a.In = append(a.In, input)
	}
}

type InputOp struct {
	OpcodeBase
}

func (a *InputOp) Execute() error {
	idx := a.parameters[0]
	if len(a.parameterModes) >= 1 && a.parameterModes[0] == RelativeParameterMode {
		idx += a.relativeBase
		if idx < 0 {
			return fmt.Errorf("encountered negative position: op %d, current position %d, negative %d", a.id, a.position, idx)
		}
	}
	a.program[idx] = a.input
	return nil
}

type OutputOp struct {
	OpcodeBase
}

func (a *OutputOp) Execute() error {
	idx, err := a.GetParameter(0)
	if err != nil {
		return err
	}
	a.output = idx
	return nil
}

func (o *OutputOp) OutputValue() (int64, bool) {
	return o.output, true
}

func ParseProgram(s string) (map[int64]int64, error) {
	errMsg := &strings.Builder{}
	val := strings.Split(s, ",")
	prog := make(map[int64]int64, len(val))

	for i, v := range val {
		j, err := strconv.ParseInt(v, 10, 64)
		if err != nil {
			fmt.Fprintf(errMsg, "index %d, value %s, error %s", i, v, err.Error())
		}
		prog[int64(i)] = j
	}
	if errMsg.Len() > 0 {
		return nil, errors.New("problems parsing program: " + errMsg.String())
	}
	return prog, nil
}

func (p *Program) ParseOpcode() (Opcode, error) {
	position := p.position
	program := p.Program
	input := p.In
	outMode := p.OutMode

	if position < int64(0) {
		return nil, fmt.Errorf("invalid negative position %d", position)
	}

	inst := program[position]

	num := []int64{}
	for i := 0; inst > 0; i++ {
		num = append(num, inst%10)
		inst /= 10
	}

	modes := []ParameterMode{}
	o := int(num[0])
	if len(num) > 1 {
		o += int(num[1]) * 10
	}
	if len(num) > 2 {
		num = num[2:]
		modes = make([]ParameterMode, len(num))
		for i := 0; i < len(modes); i++ {
			modes[i] = ParameterMode(num[i])
		}
	}

	l := int64(len(program))
	var op Opcode
	b := &OpcodeBase{}
	b.id = o
	b.outMode = outMode
	b.parameterModes = modes
	b.position = position
	b.program = program
	b.programLength = l
	b.relativeBase = p.relativeBase
	b.resultingProgramStatus = ProgramRunning

	parameterPosition := position + 1
	if o == 1 || o == 2 || o == 7 || o == 8 {
		parameters := map[int64]int64{
			0: program[parameterPosition],
			1: program[parameterPosition+1],
			2: program[parameterPosition+2],
		}
		t := &ThreeArgFuncOpcode{}
		t.OpcodeBase = *b
		t.parameters = parameters
		if o == 1 {
			a := &AddOpcode{}
			a.ThreeArgFuncOpcode = *t
			op = a
		} else if o == 2 {
			m := &MulOpcode{}
			m.ThreeArgFuncOpcode = *t
			op = m
		} else if o == 7 {
			co := &LessThanOpcode{}
			co.ThreeArgFuncOpcode = *t
			co.id = o
			op = co
		} else if o == 8 {
			co := &EqualsOpcode{}
			co.ThreeArgFuncOpcode = *t
			co.id = o
			op = co
		}
	} else if o == 3 || o == 4 {
		parameters := map[int64]int64{
			0: program[parameterPosition],
		}
		b.parameters = parameters

		if o == 3 {
			in := &InputOp{}
			in.OpcodeBase = *b
			if len(in.parameterModes) > 0 {
				for i, m := range in.parameterModes {
					if m == ImmediateParameterMode {
						in.parameterModes[i] = PositionParameterMode
					}
				}
			}
			n, ok := input.Next()
			in.input = n
			if !ok {
				in.resultingProgramStatus = ProgramAwaitingInput
			}
			op = in
		} else if o == 4 || o == 9 {
			out := &OutputOp{}
			out.OpcodeBase = *b
			out.id = o
			if outMode == FeedbackLoop {
				out.resultingProgramStatus = ProgramSentOutput
			}
			op = out
		}
	} else if o == 5 || o == 6 {
		parameters := map[int64]int64{
			0: program[parameterPosition],
			1: program[parameterPosition+1],
		}
		t := &JumpConditionOp{}
		t.OpcodeBase = *b
		t.parameters = parameters

		if o == 5 {
			jc := &JumpIfTrueOp{}
			jc.JumpConditionOp = *t
			op = jc
		} else if o == 6 {
			jc := &JumpIfFalseOp{}
			jc.JumpConditionOp = *t
			op = jc
		}
	} else if o == 9 {
		parameters := map[int64]int64{
			0: program[parameterPosition],
		}
		b.parameters = parameters

		r, err := b.GetParameter(0)
		if err != nil {
			return nil, err
		}
		p.relativeBase += r
		op = b
	} else if o == 99 {
		b.resultingProgramStatus = ProgramCompleted
		op = b
	} else {
		return nil, fmt.Errorf("received invalid opcode %d at position %d", o, position)
	}

	p.status = op.ResultingProgramStatus()
	return op, nil
}

func ExecuteProgram(s string, input []int64, outMode OutputMode) ([]int64, error) {
	program, err := ParseProgram(s)
	if err != nil {
		return nil, err
	}

	return ExecuteProgramMap(program, input, outMode)
}

func ExecuteProgramMap(program map[int64]int64, input []int64, outMode OutputMode) ([]int64, error) {
	in := &ArrayProgramInput{In: input}
	prog := &Program{Program: program, OutMode: outMode, In: in}
	return prog.Execute()
}

func (p *Program) Execute() ([]int64, error) {
	res := []int64{}

	for {
		op, err := p.ParseOpcode()
		if err != nil {
			return nil, err
		}

		if op == nil {
			return nil, fmt.Errorf("unknown op parsing error at position %d, %v", p.position, p.Program)
		}

		if p.Status() == ProgramCompleted || p.Status() == ProgramAwaitingInput {
			break
		}

		err = op.Execute()
		if err != nil {
			return nil, err
		}

		out, ok := op.OutputValue()
		if ok {
			res = append(res, out)
			p.Output = res
		}

		p.position, _ = op.NextPosition()
		if ok && p.Status() == ProgramSentOutput {
			return res, nil
		}
	}

	return res, nil
}
