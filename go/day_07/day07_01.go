package main

import (
	"bufio"
	"fmt"
	"gitlab.com/laz/adventofcode/2019/go/intcode"
)

func init() {
	intFuncs = append(intFuncs, doOne)
}

func doOne(s *bufio.Scanner) (int, error) {
	max := int64(0)
	s.Scan()
	l := s.Text()

	p, err := intcode.ParseProgram(l)
	if err != nil {
		return 0, err
	}
	tests := permutations([]int64{0, 1, 2, 3, 4})

	for _, test := range tests {
		res := []int64{0}
		for amp, t := range test {
			program := make(map[int64]int64, len(p))
			for k, v := range p {
				program[k] = v
			}
			res, err = intcode.ExecuteProgramMap(program, []int64{t, res[0]}, intcode.IgnoreOutput)
			if err != nil {
				return 0, fmt.Errorf("error processing amp %d test %v with input [%d %v]: %s", amp, test, t, res, err)
			}
		}
		if res[0] > max {
			max = res[0]
		}
	}

	return int(max), nil
}
