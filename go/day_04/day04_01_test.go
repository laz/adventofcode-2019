package main

import (
	"bufio"
	"bytes"
	"testing"
)

func TestDoOne(t *testing.T) {
	tests := map[string]int{
		"111111-111111": 1,
		"223450-223450": 0,
		"123789-123789": 0,
	}

	for test, expect := range tests {
		actual, err := doOne(bufio.NewScanner(bytes.NewBufferString(test)))

		if err != nil {
			t.Errorf("unexpected error on input %s: %s", test, err)
		}
		if actual != expect {
			t.Errorf("failed on input %s: got %d, expected %d", test, actual, expect)
		}
	}
}
