package main

import (
	"bufio"
	"gitlab.com/laz/adventofcode/2019/go/intcode"
)

func init() {
	intFuncs = append(intFuncs, doOne)
}

func doOne(s *bufio.Scanner) (int, error) {
	s.Scan()
	l := s.Text()
	r, err := intcode.ExecuteProgram(l, []int64{1}, intcode.IgnoreOutput)
	sum := int64(0)
	for _, i := range r {
		sum += i
	}
	return int(sum), err
}
