package main

import (
	"bufio"
	"bytes"
)

type color byte

const (
	black color = '0' + iota
	white
	transparent
)

var paint = map[color]string{
	black:       " ",
	white:       "\u2588",
	transparent: "x",
}

func init() {
	stringFuncs = append(stringFuncs, doTwo)
}

func doTwo(s *bufio.Scanner) (string, error) {
	width := 25
	height := 6
	count := width * height
	image := make([]color, count)
	for i := range image {
		image[i] = transparent
	}

	s.Scan()
	l := s.Text()
	for i := 0; i < len(l); i++ {
		j := i % count
		c := color(l[i])
		//fmt.Println(i, c, j, image[j])
		if image[j] == transparent && c != transparent {
			image[j] = c
		}
	}

	res := &bytes.Buffer{}
	for i, b := range image {
		res.WriteString(paint[b])
		if (i+1)%width == 0 {
			res.WriteString("\n")
		}
	}
	return res.String(), nil
}
