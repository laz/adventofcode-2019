package main

import (
	"bufio"
)

func init() {
	intFuncs = append(intFuncs, doOne)
}

func doOne(s *bufio.Scanner) (int, error) {
	prog, err := input(s)
	if err != nil {
		return 0, err
	}
	return process(prog)
}
