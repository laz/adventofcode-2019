package main

import (
	"bufio"
	"strings"
)

func init() {
	intFuncs = append(intFuncs, doTwo)
}

func doTwo(s *bufio.Scanner) (int, error) {
	m := map[string]*orbiter{"COM": &orbiter{id: "COM"}}
	o := []*orbiter{m["COM"]}
	for s.Scan() {
		l := s.Text()
		pair := strings.Split(l, ")")

		a := m[pair[0]]
		if a == nil {
			a = &orbiter{id: pair[0]}
			m[pair[0]] = a
			o = append(o, a)
		}
		b := m[pair[1]]
		if b == nil {
			b = &orbiter{id: pair[1]}
			m[pair[1]] = b
			o = append(o, b)
		}
		a.addOrbit(b)
	}

	totals := map[string]int{}
	for _, cur := range o {
		orb := m[cur.id]
		for orb != nil {
			if orb.orbitting != nil {
				totals[cur.id]++
			}
			orb = orb.orbitting
		}
	}

	common := map[string]bool{}
	found := ""
	meAndSanta := []string{"YOU", "SAN"}
	for _, id := range meAndSanta {
		orb := m[id]
		for orb != nil {
			orb = orb.orbitting
			if orb != nil {
				if common[orb.id] {
					found = orb.id
					break
				}
				common[orb.id] = true
			}
		}
	}

	tot := totals[meAndSanta[0]] + totals[meAndSanta[1]] - (totals[found] * 2) - 2
	return tot, nil
}
