package main

import (
	"bufio"
	"bytes"
	"testing"
)

func TestDoTwo(t *testing.T) {
	tests := map[string]int{
		"R8,U5,L5,D3\nU7,R6,D4,L4": 30,
		"R75,D30,R83,U83,L12,D49,R71,U7,L72\nU62,R66,U55,R34,D71,R55,D58,R83":               610,
		"R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51\nU98,R91,D20,R16,D67,R40,U7,R15,U6,R7": 410,
	}

	for test, expect := range tests {
		actual, err := doTwo(bufio.NewScanner(bytes.NewBufferString(test)))

		if err != nil {
			t.Errorf("unexpected error on input %s: %s", test, err)
		}
		if actual != expect {
			t.Errorf("failed on input %s: got %d, expected %d", test, actual, expect)
		}
	}
}
