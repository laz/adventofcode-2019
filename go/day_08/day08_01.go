package main

import (
	"bufio"
)

func init() {
	intFuncs = append(intFuncs, doOne)
}

type layer struct {
	zeroes int
	ones   int
	twos   int
}

func doOne(s *bufio.Scanner) (int, error) {
	width := 25
	height := 6

	s.Scan()
	return calc(s.Text(), width, height), nil
}

func calc(sif string, width, height int) int {
	count := width * height
	min := &layer{count, 0, 0}

	cur := &layer{count, 0, 0}
	for i, b := range sif {
		if i%count == 0 {
			if cur.zeroes < min.zeroes {
				min = cur
			}
			cur = &layer{}
		}
		switch b {
		case '0':
			cur.zeroes++
		case '1':
			cur.ones++
		case '2':
			cur.twos++
		}
	}
	return min.ones * min.twos
}
