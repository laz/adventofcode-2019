package main

import (
	"fmt"
)

type point struct {
	x int
	y int
}

func (p *point) move(dir byte, steps int) []point {
	moves := make([]point, steps)
	sign := 1
	if dir == 'D' || dir == 'L' {
		sign *= -1
	}
	if dir == 'D' || dir == 'U' {
		for i := 0; i < steps; i++ {
			moves[i] = point{p.x, p.y + (sign * i)}
		}
		p.y += sign * steps
	}
	if dir == 'L' || dir == 'R' {
		for i := 0; i < steps; i++ {
			moves[i] = point{p.x + (sign * i), p.y}
		}
		p.x += sign * steps
	}
	return moves
}

func (p *point) distance(b *point) int {
	dx := p.x - b.x
	if dx < 0 {
		dx = -dx
	}
	dy := p.y - b.y
	if dy < 0 {
		dy = -dy
	}
	return dx + dy
}

func (p *point) String() string {
	return fmt.Sprintf("%+v", *p)
}
