package main

import "fmt"

type orbiter struct {
	id        string
	orbitting *orbiter
	orbits    []*orbiter
}

func (o *orbiter) addOrbit(p *orbiter) {
	if o.orbits != nil {
		o.orbits = []*orbiter{p}
	} else {
		o.orbits = append(o.orbits, p)
	}
	p.orbitting = o
}

func (o *orbiter) String() string {
	ids := make([]string, len(o.orbits))
	for i, p := range o.orbits {
		ids[i] = p.id
	}
	id := ""
	if o.orbitting != nil {
		id = o.orbitting.id
	}
	return fmt.Sprintf("{%s %v}", id, ids)
}
