package main

import (
	"bufio"
	"strconv"
	"strings"
)

func init() {
	intFuncs = append(intFuncs, doOne)
}

func doOne(s *bufio.Scanner) (int, error) {
	tot := 0
	s.Scan()
	l := s.Text()
	rg := strings.Split(l, "-")

	start, err := strconv.Atoi(rg[0])
	if err != nil {
		return 0, err
	}

	end, err := strconv.Atoi(rg[1])
	if err != nil {
		return 0, err
	}

	for i := start; i <= end; i++ {
		num := make([]int, len(rg[1]))
		p := i
		for j := len(num) - 1; j >= 0; j-- {
			num[j] = p % 10
			p /= 10
		}
		decrease := false
		repeat := false
		for j := 0; j < len(num)-1; j++ {
			a := num[j]
			b := num[j+1]
			if b < a {
				decrease = true
				break
			}
			repeat = repeat || a == b
		}
		if !decrease && repeat {
			tot++
		}
	}

	return tot, nil
}
